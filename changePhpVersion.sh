#!/bin/bash

cd ~/sources/gesica_web/

# Fonction pour afficher un message en couleur verte
print_green() {
    echo -e "\e[1;32m$1\e[0m"
}
# Vérification du nombre d'arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <php_version> <bypass la version de php> <restart container GW>"
    exit 1
fi
php_version="$1"
restart="$2"
expected_branch="php-?8"
current_php_version=$(php -r "echo PHP_MAJOR_VERSION;")

if [ "$current_php_version" -eq "$php_version" ]; then
	print_green "PHP est déjà en version $php_version"
	exit 0
fi

# Vérification si la version PHP est égale à 8
# Vérification si le terme "php-8" ou "php8" est présent dans le nom de la branche git actuelle
if [ "$php_version" = "8" ]; then
	if $(! git branch --show-current | grep -qE $expected_branch) ; then
		print_green "Il ne s'agit pas d'une branche PHP 8\n[ Arrêt ]"
		exit 1
	fi
    version_composer="2"
    comment_char="#"
    uncomment_char=""
elif [ "$php_version" = "7" ]; then
	if $(git branch --show-current | grep -qE $expected_branch) ; then
		print_green "Il ne s'agit pas d'une branche PHP 7\n[ Arrêt ]"
		exit 1
	fi
    version_composer="1"
    comment_char=""
    uncomment_char="#"
else
    print_green "Version de PHP non supportée\n[ Arrêt ]"
    exit 1
fi

print_green "Changement de la version de PHP"
sudo update-alternatives --config php

# Modification du fichier Makefile.local en fonction de la version PHP
sed -i "s/^${comment_char}\(DOCKER_IMAGE_\)/${uncomment_char}\1/" Makefile.local
print_green "Les lignes commençant par '#DOCKER_IMAGE_' dans le fichier 'Makefile.local' ont été modifiées."

# Mise à jour de Composer
print_green "Mise à jour de composer en version <$php_version>"
sudo composer self-update --"$version_composer"

# Suppression des dossiers Vendor et Bin
print_green "Suppression des dossiers Vendor et Bin"
rm -rf Vendor Bin

# Mise à jour des dépendances
print_green "Mise à jour des dépendances"
composer install

# Redémarrage des containers
if [[ $restart ]]; then
	make stop && make start_cli
fi
