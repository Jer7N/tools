#!/bin/bash

function do_atoum_on_modified_file() {
	# Récupérer la liste des fichiers modifiés dans Git
	files=$(git diff --name-only HEAD)
	# Vérifier si des fichiers ont été modifiés
	if [ -n "$files" ]; then
		# Transformer la liste des fichiers modifiés en un format spécifié
		formatted_files=$(echo "$files" | tr '\n' ' ')
		# Filtrer les fichiers qui existent dans le répertoire "Tests/Units"
		existing_files=""
		for file in $formatted_files; do
			if [[ $file == Tests/Units/* ]]; then
				if [ -f "$file" ]; then
					if ! echo "$existing_files" | grep -q "$file"; then
						existing_files="$existing_files $file"
					fi
				fi
			else
				# Vérifier si le fichier existe dans Tests/Units et l'ajouter à la liste s'il existe
				file_in_units="Tests/Units/$file"
				if [ -f "$file_in_units" ]; then
					existing_files="$existing_files $file_in_units"
				fi
			fi
		done
		# Si des fichiers existent encore, alors lancer la commande
		if [ -n "$existing_files" ]; then
			echo -e "[Lancement des TU dans Gesica_cmd] \n\E[1;32m> \`atoum -f $existing_files\`\E[0m \n"
			docker exec -i gesica_cmd bash -c "atoum -f $existing_files"
		else
			echo "Aucun fichier modifié valide trouvé donc pas de lancement de TU"
		fi
	else
		echo "Aucun fichier modifié."
	fi
}
